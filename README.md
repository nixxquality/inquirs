Inquirs
=======

Inspired by [Inquirer on NPM](https://www.npmjs.com/package/inquirer), this is a command-line utility for use in bash scripts and such for taking user input.

Right now it only supports selecting from a list of provided items.

```
> inquirs "Choice 1" "Choice 2"
Opens a selection box between Choice 1 and Choice 2.
The exit code will be the index (0-based) of the choice.
```

```
> inquirs --table "Short\tSomething over here" "Longer key\tBlah blah"
Parses the choices as tab separated values, and outputs them properly padded.

Short      Something over here
Longer key Blah blah
```

These arguments are **totally subject to change!!**

use std::io::{self, Write};
use crossterm::{
    style::{ResetColor},
    terminal::{self, ClearType},
    event::{read, Event, KeyCode, KeyModifiers},
    cursor, style::{self},
    execute, queue, Result

};
use std::cmp;
use std::convert::TryInto;

// const POINTER: char = '❯';

fn run<W>(w: &mut W, choices: Vec<String>) -> Result<Option<usize>>
where W: Write
{
    execute!(w, terminal::EnterAlternateScreen)?;

    terminal::enable_raw_mode()?;

    let mut columns = terminal::size()?.0;
    let mut index = 0;
    let mut canceled = false;

    loop {
        queue!(w, 
            ResetColor,
            terminal::Clear(ClearType::All),
            cursor::Hide,
            cursor::MoveTo(0,0)
        )?;

        let padding: String = std::iter::repeat(' ').take(columns as usize).collect();

        for (i, choice) in choices.iter().enumerate() {
            if i == index { queue!(w, style::SetAttribute(style::Attribute::Reverse))?; }
            let line = format!("{}{}", choice, padding);
            let len = cmp::min(line.len(), columns as usize);
            queue!(w,
                style::Print(&line[..len]),
                cursor::MoveToNextLine(1),
                style::SetAttribute(style::Attribute::Reset)
            )?;
        }

        w.flush()?;

        match read()? {
            Event::Key(event) => {
                match event.code {
                    KeyCode::Up => { index = index.saturating_sub(1) }
                    KeyCode::Down => { index = cmp::min(index + 1, choices.len() - 1) }
                    KeyCode::Enter => { break }
                    KeyCode::Char('c') if event.modifiers == KeyModifiers::CONTROL => {
                        canceled = true;
                        break;
                    }
                    _ => { /* eprintln!("{:?}", event) */ }
                }
            },
            Event::Resize(newcolumns, _) => { columns = newcolumns },
            _ => {},
        }
    }
    
    execute!(w,
        style::ResetColor,
        terminal::Clear(ClearType::All),
        cursor::Show,
        terminal::LeaveAlternateScreen
    )?;

    terminal::disable_raw_mode()?;

    if canceled {
        Ok(None)
    } else {
        Ok(Some(index))
    }
}

fn padcolumns(lines: Vec<String>) -> Option<Vec<String>> {
    if lines.is_empty() { return Some(lines) }
    
    let splitlines: Vec<Vec<&str>> = lines.iter()
        .map(|s| s.split('\t').collect())
        .collect();

    let num_of_cols = splitlines[0].len();
    // todo: verify equal columns

    let padvalues: Vec<usize> = splitlines.iter()
        .map(|row| row.iter().map(|col| col.len()).collect())
        .fold(vec![0; num_of_cols], |highest, row: Vec<usize>| {
            highest.into_iter().zip(row).map(|(a, b)| cmp::max(a, b)).collect()
        });

    let paddedlines: Vec<String> = splitlines.into_iter()
        .map(|row| {
            row.iter()
                .zip(&padvalues)
                .map(|(col, width)| format!("{:<width$}", col, width = width))
                .collect::<Vec<String>>()
                .join(" ")
        })
        .collect();

    Some(paddedlines)
}

enum TakingArg {
    None,
    ExecutableName,
}

fn main() -> Result<()> {
    let mut stdout = io::stdout();
    let mut choices = vec![];
    
    let mut taking_arg = TakingArg::ExecutableName;
    let mut table = false;

    for arg in std::env::args() {
        match taking_arg {
            TakingArg::ExecutableName => {
                taking_arg = TakingArg::None;
            },
            TakingArg::None => {
                match arg.as_str() {
                    "--table" => { table = true },
                    _ => { choices.push(arg) }
                }
            }
        }
    }

    if table {
        choices = padcolumns(choices).unwrap();
    }

    if choices.is_empty() {
        std::process::exit(-2); // no choices provided
    }
    
    let choice = run(&mut stdout, choices)?;

    if let Some(choice) = choice {
        std::process::exit(choice.try_into().unwrap());
    } else {
        std::process::exit(-1); // user canceled
    }
}
